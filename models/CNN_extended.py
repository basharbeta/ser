# in this model, we treat the input MFCC psectorgram as an image, from which we extract the classes.
# model : 1 conv + 1 maxPool + flatten + softmax
# model: n * (conv + (min/max)Pool) + flatten + softmax
import torch
import torch.nn as nn
from torchsummary import summary
from SER.src import train
from torch.utils.data import DataLoader
from SER.src import dataset
import optuna
import torch
import torch.nn as nn
from torchsummary import summary
import math


class CNN_extended(nn.Module):
    def __init__(self, d_input, seq_length, n_classes, kernel_size=(3, 3), pooling: str = 'max'):
        super().__init__()
        self.params = 3 * self.get_size(self.get_size(seq_length, kernel_size[0], 2), 2, 2) * self.get_size(
            self.get_size(d_input, kernel_size[1], 2), 2, 2)
        self.pool = nn.MaxPool2d((2, 2)) if pooling == 'max' else nn.AvgPool2d((3, 3))

        self.layers = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=9, kernel_size=(15, 5), stride=(2, 2)),
            nn.ReLU(),
            self.pool,
            nn.BatchNorm2d(num_features=9),
            nn.Dropout(0.1),

            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=(3, 3), stride=(1, 1)),
            nn.ReLU(),
            self.pool,
            nn.BatchNorm2d(num_features=64),
            nn.Dropout(0.1),
            nn.Flatten(),
            nn.Linear(2048, 512),
            nn.Linear(512, 4),
            nn.Softmax(dim=-1)
        )

    def get_size(self, length, kernel, stride):
        return math.ceil((length - kernel + 1) / stride)

    def forward(self, x):
        x = torch.unsqueeze(x, dim=1)
        out = self.layers(x)
        return out




loss = nn.CrossEntropyLoss()
device = 'cuda' if torch.cuda.is_available() else 'cpu'
cnn_model1 = CNN_extended(26, 399,4)
summary(cnn_model1.to(device), (399, 26))

data = train.model_train(model=cnn_model1, model_name='cnn_model2', train_set=dataset.train_dataset,
                         val_set=dataset.validation_dataset, batch_size=16, num_epochs=50, device=device, loss_fn=loss,
                         lr=0.00001, metric=None, ext_dir='./cnn_model_bestparams2')


