import torch
import torch.nn as nn


class input_embedding(nn.Module):
    def __init__(self, d_input, d_model, stride=10, kernel_size=16):
        """
        :param d_input: feature-vector dimension of input spectogram
        :param d_model: output dimension of the embeddings
        """
        super().__init__()
        self.proj = nn.Conv2d(1, d_model, kernel_size=(d_input, kernel_size), stride=stride)

    def forward(self, x):
        """
        :param x: input of shape [B, d_input, t_length]
        return : output of shape [B, max_length, d_model]"""
        # we need to add new axis for number of channels
        x = torch.unsqueeze(x, dim=1)
        # we apply the projection layer
        x = self.proj(x)
        # we remove the height dimension, because it is always equal to 1
        x = torch.squeeze(x, dim=2)
        # x = torch.einsum('bij->bji', x)
        x = x.transpose(1, 2)
        return x


class PositionalEncoding(nn.Module):
    """
    a nn.Module wrapper to extract Position embeddings with a specific dimnesionality
    """

    def __init__(self, d_model, max_length):
        """
        :param max_length: number of columns of input embedding
        :param d_model: number of rows to represent input features of the transformer
        """
        super().__init__()
        self.max_length = max_length
        self.d_model = d_model

    def forward(self, x):
        # get the length of sequency from input
        seq_len = x.shape[1]

        even_i = torch.arange(0, self.d_model, 2).float()
        even_den = pow(10000, even_i / self.d_model)
        pos = torch.arange(seq_len).float().unsqueeze(1)

        even_pe = torch.sin(pos / even_den)
        odd_pe = torch.cos(pos / even_den)
        stacked = torch.stack([even_pe, odd_pe], dim=2)
        stacked = torch.flatten(stacked, start_dim=1, end_dim=2)
        return stacked


class MultiHeadSplit(nn.Module):
    """split the input data into multiple heads using linear layers"""

    def __init__(self, d_model: int, d_k: int, heads: int = 8, bias=False):
        """
        :param d_model: input feature dimension
        :param d_k: dimension of each head
        :param heads: number of heads to split each input sample into
        :param bias: whether to apply bias term into linear layer"""
        super().__init__()
        # copy params into local attributes of the class
        self.d_model = d_model
        self.d_k = d_k
        self.heads = heads

        # assert that numbers check
        assert (self.d_k * self.heads == self.d_model), "heads number isn't compatible with dimesions!"

        # create a linear layer to create the different heads
        self.linear = nn.Linear(d_model, heads * d_k, bias=bias)

    def forward(self, x):
        """x is of shape [-1, seq_length, d_model]"""
        # change the view of the input so that trainable features (d_model) is the last
        shape = x.shape[:-1]
        # split x into multiple heads
        x = self.linear(x)

        # split last dimension into two
        x = x.view(*shape, self.heads, self.d_k)
        x = torch.transpose(x, -2, -3)

        # return output
        return x


class SelfAttention(nn.Module):
    def __init__(self, d_model, heads, d_k, bias=False, dropout=0.1):  # dropout is set randomly
        super().__init__()

        self.d_model = d_model
        self.heads = heads
        self.d_k = d_k

        # split input into three matrecies : q: query, k: key, v: value using linear layers
        self.query = MultiHeadSplit(d_model, d_k, heads, bias)
        self.key = MultiHeadSplit(d_model, d_k, heads, bias)
        self.value = MultiHeadSplit(d_model, d_k, heads, bias)

        # define necessery activations
        self.softmax = nn.Softmax(dim=-1)  # the input to the softmax has shape (t_dim, t_dim)

        self.dropout = nn.Dropout(dropout)

        self.scale = 1 / (self.d_k) ** (0.5)

        output = nn

    def get_att(self, q, k):
        """
        q shape (B, h, q_len, d)
        k shape (B, h, k_len, d)

        :return : output of shape (B, h, q_len, k_len)
            """
        out = torch.einsum('bhqd,bhkd->bhqk', q, k)
        return out

    def forward(self, x, mask=None):
        """
        :param x : input of shape (B, seq_length, d_model)
        :param mask : mask of shape (B, seq_length)

        :return output of shape (B, seq_length, d_v)
        """

        q = self.query(x)
        k = self.key(x)
        v = self.value(x)

        # apply matmul
        att = self.get_att(q, k)

        # apply scale
        att = att * self.scale

        # apply mask
        if mask is not None:
            att = att.masked_fill(mask == 0, -1 * torch.inf)

        # apply softmax
        att = self.softmax(att)

        # apply matmul
        att = torch.einsum('bhqk,bhkd->bqhd', att, v)

        # concatenate all heads in one (bhqd -> bq(d*h)
        shape = att.shape[:-2]

        out = att.reshape(*shape, att.shape[-1] * att.shape[-2])

        return out


# next we will implement the feed forward layer

class FeedForward(nn.Module):
    def __init__(self, d_model, n_hidden, dropout=0.1):
        """
        :param n_hidden number of feed forward features in the hidden layer"""
        super().__init__()
        self.d_model = d_model
        self.n_hidden = n_hidden

        # First linear layer
        self.linear1 = nn.Linear(d_model, n_hidden)

        # Second Linear layer
        self.linear2 = nn.Linear(n_hidden, d_model)

        # dropout layer
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        # apply first linear
        x = self.linear1(x)
        x = nn.functional.relu(x)
        x = self.dropout(x)
        x = self.linear2(x)
        return x


class LayerNorm(nn.Module):
    def __init__(self, input_shape):
        super().__init__()
        self.norm = nn.LayerNorm(normalized_shape=input_shape)

    def forward(self, x):
        return self.norm(x)


# now we implement the encoder block

class EncoderBlock(nn.Module):
    """
    This layer consists of self attention layer, followed by add and norm layer, then a feed forward layer, followed by add and norm layer """

    def __init__(self, d_model, d_k, heads, n_hidden=1024, dropout=0.1, bias=False):
        super().__init__()
        self.d_model = d_model
        self.heads = heads
        self.d_k = d_k
        self.bias = bias

        # define a self attention layer
        self.self_attention = SelfAttention(d_model, heads, d_k, bias, dropout)

        # norm layer
        self.norm1 = LayerNorm(d_model)
        self.norm2 = LayerNorm(d_model)

        # feed forward layer
        self.ff = FeedForward(d_model, n_hidden=n_hidden, dropout=dropout)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        """
        x has a shape of (B, seq_length, d_model)"""

        att = self.self_attention(x)

        x = self.norm1(x + att)

        x = self.dropout(x)
        ff = self.ff(x)

        x = self.norm2(x + ff)
        x = self.dropout(x)

        return x


# the encoder sonsists of a positional embedding block + sequence of n encoder blocks


class Encoder(nn.Module):
    def __init__(self, max_length, d_model, d_k, heads, n_encoders=1, n_hidden=1024, dropout=0.1, bias=False):
        super().__init__()

        # self.pe = PositionalEncoding(d_model, max_length)

        self.enc_blocks = nn.ModuleList([
            EncoderBlock(d_model, d_k, heads, n_hidden, dropout, bias) for i in range(n_encoders)
        ])
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        """x is of shape (B, seq_length, d_model)
        :return output of shape (B, seq_length, d_model)"""

        seq_length = x[1]
        # pe = self.pe()[:seq_length,:]
        # add positional information
        x = x  # + pe

        # inject new input to encoder blocks

        for layer in self.enc_blocks:
            x = layer(x)
        return x

    # the full architechture consists of : input embedding + encoder + global average
    # we will experiment with global averaging and cls token [like BERT]


class Classifier(nn.Module):
    def __init__(self, n_classes, d_model, seq_length):
        super().__init__()
        self.average_pooling = nn.AvgPool1d(kernel_size=seq_length, stride=seq_length)
        self.flat = nn.Flatten(start_dim=1, end_dim=2)
        self.linear = nn.Linear(d_model, n_classes)
        self.softmax = nn.Softmax(dim=-1)

    def forward(self, x):
        # add channel dimension
        x = x.unsqueeze(dim=1)

        # transpose input
        x = x.transpose(-1, -2)

        # flatten input
        x = self.flat(x)

        # apply avgPool
        x = self.average_pooling(x)

        # remove added dimension
        x = x.squeeze(dim=-1)

        # apply linear layer
        x = self.linear(x)

        # apply softmax
        x = self.softmax(x)

        return x


class SERT(nn.Module):
    def __init__(self, d_input,
                 max_length,
                 d_model,
                 d_k,
                 heads,
                 n_classes,
                 n_encoders=1,
                 n_hidden=1024,
                 dropout=0.1,
                 bias=False,
                 stride=10,
                 kernel_size=16):
        super().__init__()
        self.input = input_embedding(d_input, d_model, stride, kernel_size)
        self.enc_layers = nn.ModuleList([
            Encoder(max_length,
                    d_model,
                    d_k,
                    heads,
                    n_encoders=1,
                    n_hidden=1024,
                    dropout=0.1,
                    bias=False) for _ in range(n_encoders)
        ])
        self.output = Classifier(n_classes, d_model, max_length)

    def forward(self, x):
        x = self.input(x)
        for layer in self.enc_layers:
            x = layer(x)

        x = self.output(x)
        return x
