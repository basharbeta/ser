
from SER.src import train, dataset
import optuna
import torch
import torch.nn as nn
from torchsummary import summary
import math


class CNN_SER(nn.Module):
    def __init__(self, d_input, seq_length, n_classes, kernel_size=(3, 3), pooling: str = 'max'):
        super().__init__()
        self.params = 10 * self.get_size(self.get_size(seq_length, kernel_size[0], 2), 2, 2) * self.get_size(
            self.get_size(d_input, kernel_size[1], 2), 2, 2)
        self.pool = nn.MaxPool2d((2, 2)) if pooling == 'max' else nn.AvgPool2d((2, 2))

        self.layers = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=10, kernel_size=kernel_size, stride=(2, 2)),
            nn.ReLU(),
            self.pool,
            nn.Flatten(),
            nn.Dropout(0.01),
            nn.Linear(self.params, n_classes),
            nn.Softmax(dim=-1)
        )

    def get_size(self, length, kernel, stride):
        return math.ceil((length - kernel + 1) / stride)

    def forward(self, x):
        x = torch.unsqueeze(x, dim=1)
        out = self.layers(x)
        return out


def find_best_params():
    def objective(trial):
        batch_size = trial.suggest_categorical("batch_size", [16, 32, 64])
        lr = trial.suggest_categorical("lr", [0.01, 0.001, 0.0001])
        kernel_size = trial.suggest_categorical("kernel_size", [(3, 3), (5, 5), (7, 7)])
        pool = trial.suggest_categorical("pool", ['max', 'avg'])
        loss = nn.CrossEntropyLoss()
        device = 'cuda' if torch.cuda.is_available() else 'cpu'
        cnn_model = CNN_SER(d_input=13, seq_length=399, n_classes=4, kernel_size=kernel_size, pooling=pool)
        data = train.model_train(cnn_model, "cnn", dataset.train_dataset, dataset.validation_dataset,
                                 num_epochs=50, device=device, loss_fn=loss, batch_size=batch_size, lr=lr, metric=None,
                                 ext_dir='./cnn_best_model')

        # optimize for train_acc
        return data[1][-1]

    study = optuna.create_study(direction='maximize')
    study.optimize(objective, n_trials=25)
    best_params = study.best_params
    return best_params


loss = nn.CrossEntropyLoss()
device = 'cuda' if torch.cuda.is_available() else 'cpu'
cnn_model = CNN_SER(d_input=26, seq_length=399, n_classes=4, kernel_size=(15, 5))
summary(cnn_model.to(device), (399,26))

data = train.model_train(model=cnn_model, model_name='cnn_model3', train_set=dataset.train_dataset,
                         val_set=dataset.validation_dataset, batch_size=16, num_epochs=10, device=device, loss_fn=loss,
                         lr=0.00001, metric=None, ext_dir='./cnn_model_bestparams3')


