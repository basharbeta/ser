import torch
import torch.nn as nn
from torchsummary import summary
from SER.src import train
from torch.utils.data import DataLoader
from SER.src import dataset
import optuna

hyperparameters = {
    'batch_size': 32,
    'epochs': 10,
    'lr': 0.0001,

}


class FeedForward(nn.Module):
    """apply a simple feed forward + softmax to get the labels"""

    def __init__(self, n_classes, d_model, seq_length):
        super().__init__()
        self.average_pooling = nn.AvgPool1d(kernel_size=seq_length, stride=seq_length)
        self.flat = nn.Flatten(start_dim=1, end_dim=2)
        self.linear = nn.Linear(d_model, n_classes).float()
        self.softmax = nn.Softmax(dim=-1)

    def forward(self, x):
        """input shape is [B, seq_length, d_model]"""

        x = torch.unsqueeze(x, dim=1)
        x = x.transpose(-1, -2)
        x = self.flat(x)

        x = self.average_pooling(x)

        x = torch.squeeze(x, dim=-1)

        x = self.linear(x)

        x = self.softmax(x)

        return x


def find_best_params():
    def objective(trial):
        batch_size = trial.suggest_categorical("batch_size", [16, 32, 64])
        lr = trial.suggest_categorical("lr", [0.01, 0.001, 0.0001])
        model_feed_forward = FeedForward(n_classes=4, d_model=13, seq_length=399)
        data = train.model_train(model_feed_forward, "feed_forward", dataset.train_dataset, dataset.validation_dataset,
                                 num_epochs=5, device=device, loss_fn=loss, batch_size=batch_size, lr=lr, metric=None,
                                 ext_dir='./feed_forward_best_params')

        # optimize for train_acc
        return data[1][-1]

    study = optuna.create_study(direction='maximize')
    study.optimize(objective, n_trials=10)
    best_params = study.best_params
    return best_params


loss = nn.CrossEntropyLoss()
device = 'cuda' if torch.cuda.is_available() else 'cpu'
model_feed_forward = FeedForward(n_classes=4, d_model=13, seq_length=399)
summary(model_feed_forward.to(device), (399, 13))
data = train.model_train(model=model_feed_forward, model_name='feed_forward2', train_set=dataset.train_dataset,
                         val_set=dataset.validation_dataset, num_epochs=10, device=device, loss_fn=loss, batch_size=16,
                         lr=0.0001, ext_dir='./feed_froward2', optim='Adam', metric="accuracy")

