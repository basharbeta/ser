import torch
import torchaudio
import numpy as np
from python_speech_features import mfcc
from SER.src import hyperparameters as hp


def extract_MFCC_delta(waveform, sr, win_size=0.025, stride=0.01, n_mfcc=26):
    """
     extract 13 MFCC  as low level descriptors
    :param track_path: path of the audio track
    :param win_size: length of the hamming window (in seconds)
    :param stride: stride length (in seconds)
    :param n_mfcc: number of desired mel frequency cepstral coefficients
    :returns : spectogram of mfcc+elta features
    """

    # specifty parameters of mfcc
    n_fft = int(win_size * sr)
    hop_length = int(stride * sr)

    # get mfcc, deltas

    MFCC = mfcc(waveform, sr ,numcep=n_mfcc, nfilt=26, nfft=n_fft)

    features = np.array(MFCC)

    return features


def extract_mel_bins(waveform, sr, win_size=0.025, stride=0.01, n_mels=64):
    '''extract MEL frequency bins as features of the waveform'''
    mel_spectorgram = torchaudio.transforms.MelSpectrogram(sample_rate=sr,
                                                           n_fft=int(win_size * sr),
                                                           hop_length=int(stride * sr),
                                                           n_mels=n_mels)
    features = mel_spectorgram(waveform)
    features = np.array(features)
    features = np.squeeze(features, axis=0)
    return features
