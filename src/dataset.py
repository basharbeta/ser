import pandas as pd
import torch
import torchaudio
from torch.utils.data import Dataset
from SER.src import feature_extraction
from SER.src import hyperparameters as hp



class SER_dataset(Dataset):
    def pad_cut_if_necessary(self, waveform):
        # check length of wave form
        wav_length = waveform.shape[1]

        # if longer, cut the waveform
        if wav_length > self.num_samples:
            waveform = waveform[:, :self.num_samples]

        # if shorter, pad the waveform with zeros
        if wav_length < self.num_samples:
            num_missing_samples = self.num_samples - wav_length
            waveform = torch.nn.functional.pad(waveform, (0, num_missing_samples))

        return waveform

    def __init__(self, json_config_file, transformation, num_samples=hp.NUM_SAMPLES, validation_session="Session5",
                 is_val: bool = False):
        """
        :param json_config_file : .json file which includes tracks paths and their labels
        :param transformation : a function pointer that will be used to extract features from input audio signals
        :num_samples : total number of samples allowed for the input
        :validation_session : the folder that will be used for validation while training the model
        : is_val: a flag that is set when we need to extract validation dataset"""

        df = pd.read_json(json_config_file)
        if is_val:
            df = df[df['filenames'].str.contains(validation_session)]
        else:
            df = df[df['filenames'].str.contains(validation_session) == False]
        self.num_samples = num_samples
        self.transformation = transformation
        self.df = df

    def __getitem__(self, index):

        # get waveform from current index
        waveform, sr = torchaudio.load(self.df.iloc[index]['filenames'], normalize=True)
        resampler = torchaudio.transforms.Resample(orig_freq=sr, new_freq=hp.SAMPLE_RATE)
        waveform = resampler(waveform)
        waveform = waveform
        waveform = self.pad_cut_if_necessary(waveform)
        features = self.transformation(waveform, sr)

        # get label of the current index
        label = self.df.iloc[index]['labels']

        return features, label

    def __len__(self):
        return len(self.df)


train_dataset = SER_dataset(hp.JSON_PATH,
                            transformation=feature_extraction.extract_MFCC_delta,
                            validation_session='Session5', is_val=False)

validation_dataset = SER_dataset(hp.JSON_PATH,
                                 transformation=feature_extraction.extract_MFCC_delta,
                                 validation_session='Session5', is_val=True)


