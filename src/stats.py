import matplotlib.pyplot as plt
import json
def display_stats(json_file):
    data = None
    with open(json_file, 'r') as fp:
        data = json.load(fp)

    train_loss = data['train_loss']
    train_acc = data['train_acc']
    val_loss = data['validation_loss']
    val_acc = data['validation_acc']


    fig, ax = plt.subplots(2)
    ax[0].plot(train_loss, label='train')
    ax[0].plot(val_loss, label='validation')
    ax[0].set_ylabel('loss')
    ax[0].legend(loc='upper left')
    ax[1].plot(train_acc, label='train')
    ax[1].plot(val_acc, label='validation')
    ax[1].set_ylabel('UA')
    ax[1].set_xlabel('epoch')
    plt.show()


display_stats('../models/cnn_model3.json')