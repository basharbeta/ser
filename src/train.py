import time
import tqdm
import torch
import torch.nn as nn
from torch.utils.data import DataLoader, Dataset
import json


def model_train(model: nn.Module, model_name: str, train_set: Dataset, val_set: Dataset, num_epochs: int,
                device: str, loss_fn, metric, batch_size: int, lr: float, ext_dir: str, optim: str = "Adam", scheduler=None):
    """returns information about the model, the results of training
    :param ext_dir: directory to save the state of the best performing model according to the metric function
    """
    optimizer = None
    if optim == "Adam":
        optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    if scheduler is not None:
        scheduler = torch.optim.lr_scheduler.LinearLR(optimizer, 1.0, 0.01, total_iters=10)

    train_dataset = DataLoader(train_set, batch_size=batch_size, shuffle=True)
    val_dataset = DataLoader(val_set, batch_size=len(val_set), shuffle=True)

    print(
        f"start training with :\n total epochs: {num_epochs}\n batch size: {train_dataset.batch_size}\n total batches: {len(train_dataset)}")

    # set a timer to measure the training time
    start_timer = time.time()

    model = model.to(device)

    # log stride = 10% of the number of batches to loop through, this is used to print results while training
    log_stride = int(0.1 * len(train_dataset))

    # save the best performing model
    max_acc = 0.0

    # save metrics for logging
    train_loss = []
    train_acc = []
    eval_loss = []
    eval_acc = []

    for epoch in tqdm.tqdm(range(num_epochs)):
        # we will calculate average loss per epoch, average accuracy per epoch
        epoch_loss = 0.0
        epoch_acc = 0.0
        epoch_correct = 0

        # average metrics per log_stride
        stride_loss = 0.0
        stride_correct = 0
        stride_acc = 0.0

        model.train()
        for it, (X, y) in enumerate(train_dataset):
            X = X.to(device).float()
            y = y.to(device)

            y_pred = model(X).to(device)

            # calculate the loss
            loss = loss_fn(y_pred, y)
            stride_loss += loss
            epoch_loss += loss

            # get correct predictions
            predictions = torch.argmax(y_pred, dim=1)

            batch_correct = (y == predictions).sum().item()
            stride_correct += batch_correct
            epoch_correct += batch_correct

            # zero grad the optim.
            optimizer.zero_grad()

            loss.backward()

            optimizer.step()

            if (it + 1) % log_stride == 0:
                # print results
                print(
                    f"epoch[{epoch + 1}/{num_epochs}] | batch[{it + 1}/{len(train_dataset)}] train_loss: {stride_loss / (log_stride):.4f} | accuracy: {stride_correct / (log_stride * batch_size)}")

                # send scalars to tensorboard

                # reset parameters
                stride_loss = 0.0
                stride_correct = 0
        if scheduler is not None:
            scheduler.step()
        # save epoch training metrics:
        train_loss.append(epoch_loss.item() / len(train_dataset))
        train_acc.append(epoch_correct / (batch_size * len(train_dataset)))

        print("validation:")
        model.eval()

        with torch.inference_mode():
            val_loss = 0.0
            val_correct = 0.0
            for (X_test, y_test) in val_dataset:
                X_test = X_test.to(device).float()
                y_test = y_test.to(device)

                y_pred = model(X_test)
                val_loss += loss_fn(y_pred, y_test)
                predictions = torch.argmax(y_pred, dim=1)

                val_correct += (predictions == y_test).sum().item()

            eval_loss.append(val_loss.item() / len(val_dataset))
            eval_acc.append(val_correct / (len(val_dataset) * val_dataset.batch_size))

        print(
            f"epoch: {epoch} summary:\n train_loss: {train_loss[-1]}, validation loss: {eval_loss[-1]}, train accuracy: {train_acc[-1]}, validation accuracy :{eval_acc[-1]}")

        # update best model
        if eval_acc[-1] > max_acc:
            max_acc = eval_acc[-1]
            torch.save(model.state_dict(), ext_dir)

    total_time = (time.time() - start_timer)

    summary = {'train_acc': train_acc, 'train_loss': train_loss, 'validation_acc': eval_acc,
               'validation_loss': eval_loss}
    print(summary)
    with open(model_name + ".json", "w") as f:
        json.dump(summary, f)
        print(f"saved summary in {model_name + '.json'}")
    print(train_acc)
    return train_loss, train_acc, eval_loss, eval_acc
