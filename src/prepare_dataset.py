# prepare json file that stores filenames, genders, and labels from all sessions of the dataset
import os
import tqdm
import re
import torchaudio
import json

import hyperparameters as hp

y = "angry, happy, sad, neutral".split(",")
y = sorted(y)
y = [i.strip() for i in y]
x = [f[0:3] for f in y]
x = dict.fromkeys(x)
index_to_label = {}
for i, j in enumerate(x.keys()):
    x[j] = i
    index_to_label[i] = j
label_index = x




def prepare_json(src_path, trg_path):
    '''read dataset folder to extract a json file containing the filenames, gender, and label of each sentence'''

    global filenames_labels
    filenames_labels = {
        "filenames": [],
        "genders": [],
        "labels": []
    }

    for dir_path, dir_names, filenames in os.walk(src_path):
        # if current dir is Session_i
        folder_names = dir_path.split(sep="/")
        if folder_names[-1] == "EmoEval":
            for f in tqdm.tqdm(filenames, desc="Reading Folder: {}".format(dir_path), ncols=140):
                if f.split(".")[-1] == "txt":
                    current_path = os.path.join(dir_path, f)
                    with open(current_path, "r") as fr:
                        for l in fr:
                            reg = re.findall("\[\d+.\d+ \- \d+.\d+\]", l)

                            if reg is not None:
                                splits = l.split()
                                if len(splits) < 5:
                                    continue
                                sentence_path = splits[3]
                                emo = splits[4]

                                # merge excited with happy
                                if emo == 'exc':
                                    emo = 'hap'

                                if emo in label_index:
                                    try:

                                        track_path = os.path.join(dir_path, '../Sentences', sentence_path + ".wav")
                                        torchaudio.load(track_path)
                                        filenames_labels["filenames"].append(track_path)
                                        filenames_labels["genders"].append(sentence_path[-4])
                                        filenames_labels["labels"].append(label_index[emo])
                                    except:
                                        pass

        if dir_path.split(sep="/")[-1][:-1] == "Session":
            session_number = dir_path.split(sep="/")[-1][-1]
            current_path = os.path.join(dir_path, 'dialog/EmoEvaluation/Categorical')

    os.makedirs(os.path.dirname(track_path), exist_ok=True)
    os.makedirs(os.path.dirname(trg_path), exist_ok=True)
    with open(trg_path, 'w+') as fp:
        json.dump(filenames_labels, fp, indent=4)

    print("output location: {}".format(trg_path))
    return filenames_labels["labels"]


files = prepare_json(hp.ROOT_PATH, hp.JSON_PATH)
